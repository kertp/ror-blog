class AddOpenColumnToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :open, :boolean
  end
end

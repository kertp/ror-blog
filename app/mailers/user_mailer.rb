class UserMailer < ActionMailer::Base
  default :from => "kertpjatkin@gmail.com"
  
  def comment_created(post)
    @post = post
    mail(:to => "#{post.email}", :subject => "Sinu postitust on kommenteeritud")
  end
end
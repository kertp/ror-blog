class Post < ActiveRecord::Base
  attr_accessible :content, :name, :title, :tags_attributes, :email, :open
 
  validates :name,  :presence => true
  validates :title, :presence => true,
                    :length => { :minimum => 5 }

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, format: { with: VALID_EMAIL_REGEX}
  has_many :comments, :dependent => :destroy
  has_many :tags
 
  accepts_nested_attributes_for :tags, :allow_destroy => :true,
    :reject_if => proc { |attrs| attrs.all? { |k, v| v.blank? } }
end